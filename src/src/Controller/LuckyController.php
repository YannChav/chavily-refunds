<?php
// src/Controller/LuckyController.php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LuckyController extends AbstractController
{
    #[Route(
        "/lucky/number",
        name:"app_lucky_number",
        condition: "context.getMethod() in ['PUT']")]
    public function number(): Response
    {
        $number = random_int(0, 100);

        return $this->render('lucky/number.html.twig', [
            'number' => $number,
        ]);
    }

    #[Route(
        "/lucky/number",
        name:"app_lucky_number",
        condition: "context.getMethod() in ['GET']")]
    public function numberGet(): Response
    {
        $number = random_int(100, 1000);

        return $this->render('lucky/number.html.twig', [
            'number' => $number,
        ]);
    }
}