# Chavily Refunds

Ce projet a pour objectif de fournir un outil permettant de suivre les dépenses et les remboursements au sein d'un groupe de personnes.

## Prérequis

Pour fonctionner, ce projet nécessite **Docker Engine**, version *25.0.2* au minimum.

## Installation

Pour installer ce projet, vous devez :
- Exécuter la commande `docker compose up --build -d`
- Exécuter la commande `docker compose run --rm composer install`

## Démarrage

---

#### Liens utiles

- [Documentation installation **Docker Engine**](https://docs.docker.com/engine/install/)
- [Tuto Docker et Symfony](https://oclock.io/deployer-docker-developper-symfony)